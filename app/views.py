# coding: utf8
import os, subprocess, datetime
from app import app
from flask import render_template, request, redirect, url_for

chemin = os.path.join(os.path.dirname(__file__), "static")

@app.route('/')
def homelist():
   return render_template('main.html', fichiers=sorted(os.listdir(chemin)))

@app.route('/scanne')
def scanne():
   return render_template('scanne.html')

@app.route('/scan')
def scan():
    p = os.path.join(chemin, datetime.datetime.now().isoformat() + ".jpg")
    subprocess.run([os.path.join(os.path.dirname(__file__), "sc"), p])
    return redirect(url_for("homelist"))

@app.route('/pdfs')
def pdfslist():
   return render_template('pdfs.html', fichiers=sorted(os.listdir(chemin)))

@app.route('/convertit')
def convertit():
   return render_template('convertit.html', fichiers=sorted(os.listdir(chemin)))

@app.route('/convert')
def convert():
    p = os.path.join(chemin ,datetime.datetime.now().isoformat()+".pdf")
    re = ["convert"]
    for k in sorted(request.args):
        re.append(os.path.join(chemin ,k))
    re.append(p)
    subprocess.run(re)
    return redirect(url_for("pdfslist"))

@app.route('/retouche')
def retouche():
   return render_template('retouche.html', fichiers=sorted(os.listdir(chemin)))

@app.route('/retou/<commande>/<fichier>')
def retou(commande = "" , fichier = ""):
    re = []
    if commande == "sup":
        re.append("rm")
        re.append(os.path.join(chemin, fichier))
        subprocess.run(re)
    elif commande == "piv90g":
        re.append("convert")
        re.append("-rotate")
        re.append("-90")
        re.append(os.path.join(chemin, fichier))
        re.append(os.path.join(chemin, fichier[:-4] + "r.jpg"))
        subprocess.run(re)
        subprocess.run(["rm", os.path.join(chemin, fichier)])
    elif commande == "piv180":
        re.append("convert")
        re.append("-rotate")
        re.append("180")
        re.append(os.path.join(chemin, fichier))
        re.append(os.path.join(chemin, fichier[:-4] + "r.jpg"))
        subprocess.run(re)
        subprocess.run(["rm", os.path.join(chemin, fichier)])
    elif commande == "piv90d":
        re.append("convert")
        re.append("-rotate")
        re.append("90")
        re.append(os.path.join(chemin, fichier))
        re.append(os.path.join(chemin, fichier[:-4] + "r.jpg"))
        subprocess.run(re)
        subprocess.run(["rm", os.path.join(chemin, fichier)])
    return redirect(url_for("retouche"))
